var mongoose = require('mongoose');
var Lokacija = mongoose.model('Lokacija');

var vrniJsonOdgovor = function(odgovor, status, vsebina) {
  odgovor.status(status);
  odgovor.json(vsebina);
};

module.exports.komentarjiKreiraj = function(zahteva, odgovor) {
  vrniJsonOdgovor(odgovor, 200, {"status": "uspešno"});
};

module.exports.komentarjiPreberiIzbranega = function(zahteva, odgovor) {
  if (zahteva.params && zahteva.params.idLokacije && zahteva.params.idKomentarja) {
    Lokacija
      .findById(zahteva.params.idLokacije)
      .select('naziv komentarji')
      .exec(
        function(napaka, lokacija) {
          var rezultat, komentar;
          if (!lokacija) {
            vrniJsonOdgovor(odgovor, 404, { "sporočilo": 
              "Ne najdem lokacije s podanim enoličnim identifikatorjem idLokacije." });
            return;
          } else if (napaka) {
            vrniJsonOdgovor(odgovor, 404, napaka);
            return;
          }
          if (lokacija.komentarji && lokacija.komentarji.length > 0) {
            komentar = lokacija.komentarji.id(zahteva.params.idKomentarja);
            if (!komentar) {
              vrniJsonOdgovor(odgovor, 404, { "sporočilo": 
                "Ne najdem komentarja s podanim enoličnim identifikatorjem idKomentarja"});
            } else {
              rezultat = {
                lokacija: { naziv: lokacija.naziv, id: zahteva.params.idLokacije },
                komentar: komentar
              };
              vrniJsonOdgovor(odgovor, 200, rezultat);
            }
          } else {
            vrniJsonOdgovor(odgovor, 404, { "sporočilo": "Ne najdem nobenega komentarja."});
          }
        }
      )
  } else {
    vrniJsonOdgovor(odgovor, 404, { "sporočilo": 
      "Ne najdem zapisa, oba enolična identifikatorja idLokacije in idKomentarja sta zahtevana."});
  }
};

module.exports.komentarjiPosodobiIzbranega = function(zahteva, odgovor) {
  vrniJsonOdgovor(odgovor, 200, {"status": "uspešno"});
};

module.exports.komentarjiIzbrisiIzbranega = function(zahteva, odgovor) {
  vrniJsonOdgovor(odgovor, 200, {"status": "uspešno"});
};