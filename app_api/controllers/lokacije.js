var mongoose = require('mongoose');
var Lokacija = mongoose.model('Lokacija');

var vrniJsonOdgovor = function(odgovor, status, vsebina) {
  odgovor.status(status);
  odgovor.json(vsebina);
};

module.exports.lokacijeSeznamPoRazdalji = function(zahteva, odgovor) {
  var lng = parseFloat(zahteva.query.lng);
  var lat = parseFloat(zahteva.query.lat);
  var razdalja = parseFloat(zahteva.query.maxRazdalja);
  razdalja = isNaN(razdalja) ? 20 : razdalja;
  var tocka = {
    type: "Point",
    coordinates: [lng, lat]
  };
  var geoParametri = {
    spherical: true,
    maxDistance: razdalja * 1000,
    num: 10
  };
  if ((!lng && lng !== 0) || (!lat && lat !== 0)) {
    vrniJsonOdgovor(odgovor, 404, { "sporočilo": "Parametra lng and lat sta obvezna." });
    return;
  }
  Lokacija.geoNear(tocka, geoParametri, function(napaka, rezultati) {
    var lokacije = [];
    if (napaka) {
      vrniJsonOdgovor(odgovor, 404, napaka);
    } else {
      rezultati.forEach(function(dokument) {
        lokacije.push({
          razdalja: dokument.dis / 1000,
          naziv: dokument.obj.naziv,
          naslov: dokument.obj.naslov,
          ocena: dokument.obj.ocena,
          lastnosti: dokument.obj.lastnosti,
          _id: dokument.obj._id
        });
      });
      vrniJsonOdgovor(odgovor, 200, lokacije);
    }
  });
};

module.exports.lokacijeKreiraj = function(zahteva, odgovor) {
  vrniJsonOdgovor(odgovor, 200, {"status": "uspešno"});
};

module.exports.lokacijePreberiIzbrano = function(zahteva, odgovor) {
  if (zahteva.params && zahteva.params.idLokacije) {
    Lokacija
      .findById(zahteva.params.idLokacije)
      .exec(function(napaka, lokacija) {
        if (!lokacija) {
          vrniJsonOdgovor(odgovor, 404, { "sporočilo": 
            "Ne najdem lokacije s podanim enoličnim identifikatorjem idLokacije." });
          return;
        } else if (napaka) {
          vrniJsonOdgovor(odgovor, 404, napaka);
          return;
        }
        vrniJsonOdgovor(odgovor, 200, lokacija);
      });    
  } else {
    vrniJsonOdgovor(odgovor, 404, { "sporočilo": 
      "Manjka enolični identifikator idLokacije"});
  }
};

module.exports.lokacijePosodobiIzbrano = function(zahteva, odgovor) {
  vrniJsonOdgovor(odgovor, 200, {"status": "uspešno"});
};

module.exports.lokacijeIzbrisiIzbrano = function(zahteva, odgovor) {
  vrniJsonOdgovor(odgovor, 200, {"status": "uspešno"});
};